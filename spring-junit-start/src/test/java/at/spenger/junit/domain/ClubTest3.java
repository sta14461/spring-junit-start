package at.spenger.junit.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest3 {



	
	/*
	 * TEIL1
	*/
	@Test
	public void testEnter() {
		Club c=new Club();
		Person p=new Person("Dominik", "Stalzer", LocalDate.now(), Sex.MALE);
		boolean erg=c.enter(p);
		boolean erwartet=true;
		assertEquals(erwartet,erg);

	}

	@Test
	public void testNumberOf() {
		Club a=new Club();
		int erg=a.numberOf();
		int erwartet=0;
		assertEquals(erwartet, erg);
	}


	@Test
	//Fehler gefunden und gel�st
	//wie im Unterricht besprochen, wird genauer aufs Alter geachtet, denn wenn man dies n�chstes jahr testen w�rde, dann gehts nicht mehr
	public void testAverage(){
		Club c = new Club();
		Person p1 = new Person("Dominik", "Stalzer", LocalDate.now().minusYears(20), Sex.MALE );
		Person p2 = new Person("Heinrich", "Horst", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );

		c.enter(p1);
		c.enter(p2);

		double erg = c.averageAge();
		double erwartet = 14.5;
		assertEquals(erwartet, erg, 1);
	}

	@Test
	public void testLeave() {
		Club c=new Club();
		Person p=new Person("Dominik", "Stalzer", LocalDate.now(), Sex.MALE);
		c.enter(p);
		boolean erg=c.austreten(p);
		boolean erwartet=true;
		assertEquals(erwartet,erg);

	}
	@Test
	public void testLeaveAll() {
		Club c=new Club();
		Person p1=new Person("Dominik", "Stalzer", LocalDate.now(), Sex.MALE);
		Person p2=new Person("Hans", "Huber", LocalDate.now(), Sex.MALE);
		c.enter(p1);
		c.enter(p2);
		boolean erg=c.alleAustreten();
		boolean erwartet=true;
		assertEquals(erwartet,erg);

	}
	@Test
	//Fertig Verbessert von vorheriger Abgabe
	public void testaeltester() {
		Club c = new Club();
		Person p1 = new Person("Susi", "Salz", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Horst", "Affe", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Peter", "Griffin", LocalDate.now().minusYears(20).minusDays(5), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		Person erwartet = p3;
		Person erg = c.aeltester();

		assertEquals(erwartet, erg);
	} 

	
	//Folgendes wurde mit Stream gel�st-->  noch TEIL1!!!
	@Test
	public void testSortNameStream()
	{
		Club a=new Club();
		a.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-27"), Person.Sex.MALE));
		a.enter(new Person("James", "Bonda", LocalDate.parse("1950-09-27"), Person.Sex.MALE));
		a.enter(new Person("James", "Bondc", LocalDate.parse("1950-09-27"), Person.Sex.MALE));
		a.enter(new Person("James", "Bondd", LocalDate.parse("1950-09-27"), Person.Sex.MALE));
		a.sortNameStream();
		List<Person> l=a.getPersons();

		assertTrue(l.get(0).getLastName().equals("Bonda"));
		assertTrue(l.get(1).getLastName().equals("Bondc"));
		assertTrue(l.get(2).getLastName().equals("Bondd"));
		assertTrue(l.get(3).getLastName().equals("Bonde"));
	}

	/*
	 * TEIL2
	 */
	
	@Test
	public void testFrauenAnz()
	{
		Club c=new Club();
		c.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-01"), Person.Sex.MALE));
		c.enter(new Person("James", "Bonda", LocalDate.parse("1950-09-05"), Person.Sex.FEMALE));
		c.enter(new Person("James", "Bondc", LocalDate.parse("1950-09-03"), Person.Sex.MALE));
		c.enter(new Person("James", "Bondd", LocalDate.parse("1950-09-02"), Person.Sex.MALE));

		
		assertEquals(c.countAnzFrauen(), 1);
	}

	@Test
	public void testSortGeburtstagStringStream()
	{
		Club a=new Club();
		a.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-01"), Person.Sex.MALE));
		a.enter(new Person("James", "Bonda", LocalDate.parse("1950-09-05"), Person.Sex.MALE));
		a.enter(new Person("James", "Bondc", LocalDate.parse("1950-09-03"), Person.Sex.MALE));
		a.enter(new Person("James", "Bondd", LocalDate.parse("1950-09-02"), Person.Sex.MALE));
		a.sortAlterStream();

		List<Person> l=a.getPersons();

		assertTrue(l.get(0).getBirthdayString().equals("1950.09.01"));
		assertTrue(l.get(1).getBirthdayString().equals("1950.09.02"));
		assertTrue(l.get(2).getBirthdayString().equals("1950.09.03"));
		assertTrue(l.get(3).getBirthdayString().equals("1950.09.05"));

	}
	@Test
	public void testSortGeburtstagDateStream()
	{
		Club a=new Club();
		a.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-01"), Person.Sex.MALE));
		a.enter(new Person("James", "Bonda", LocalDate.parse("1950-09-05"), Person.Sex.MALE));
		a.enter(new Person("James", "Bondc", LocalDate.parse("1950-09-03"), Person.Sex.MALE));
		a.enter(new Person("James", "Bondd", LocalDate.parse("1950-09-02"), Person.Sex.MALE));
		a.sortAlterDateStream();

		List<Person> l=a.getPersons();

		assertTrue(l.get(0).getBirthday().equals(LocalDate.parse("1950-09-01")));
		assertTrue(l.get(1).getBirthday().equals(LocalDate.parse("1950-09-02")));
		assertTrue(l.get(2).getBirthday().equals(LocalDate.parse("1950-09-03")));
		assertTrue(l.get(3).getBirthday().equals(LocalDate.parse("1950-09-05")));

	}

	@Test
	public void testAustretenStream()
	{
		Club c=new Club();
		c.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-01"), Person.Sex.MALE));
		c.enter(new Person("James", "Bonda", LocalDate.parse("1950-09-05"), Person.Sex.MALE));
		c.enter(new Person("James", "Bondc", LocalDate.parse("1950-09-03"), Person.Sex.MALE));
		c.enter(new Person("James", "Bondd", LocalDate.parse("1950-09-02"), Person.Sex.MALE));

		long count = c.getPersons().stream().filter(p -> p.getLastName().equals("Bonde")).count();

		c.austreten(c.getPersons().get(0));

		long countnachher = c.getPersons().stream().filter(p -> p.getLastName().equals("Bonde")).count();

		assertNotEquals(count, countnachher);

	}
	@Test
	public void testEintretenStream()
	{

		Club c=new Club();

		long count = c.getPersons().stream().filter(p -> p.getLastName().equals("Bonde")).count();

		c.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-01"), Person.Sex.MALE));

		long countnachher = c.getPersons().stream().filter(p -> p.getLastName().equals("Bonde")).count();

		assertNotEquals(count, countnachher);
	}

	@Test
	public void testAverageStream()
	{
		Club c=new Club();
		c.enter(new Person("James", "Bonde", LocalDate.parse("1950-09-01"), Person.Sex.MALE));
		c.enter(new Person("James", "Bonda", LocalDate.parse("1950-09-05"), Person.Sex.MALE));
		c.enter(new Person("James", "Bondc", LocalDate.parse("1950-09-03"), Person.Sex.MALE));
		c.enter(new Person("James", "Bondd", LocalDate.parse("1950-09-02"), Person.Sex.MALE));

		float count = 0;
		float sumAlter = 0;

		for(Person p : c.getPersons())
		{
			count++;
			sumAlter += p.getAge();
		}
		float erg = sumAlter/count;

		assertEquals(c.averageStream(), erg, 0.01f);


	}


	

}