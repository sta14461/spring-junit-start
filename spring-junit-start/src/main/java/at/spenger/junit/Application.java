package at.spenger.junit;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import at.spenger.junit.domain.Person;


@Import(DefaultConfig.class)
public class Application implements CommandLineRunner {
//hier wird die application gestartet
	public static void main(String[] args) {
		new SpringApplicationBuilder(Application.class)
	    .showBanner(false)
	    .logStartupInfo(false)
	    .run(args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		
		System.out.println("p:" + p);
	}

}
