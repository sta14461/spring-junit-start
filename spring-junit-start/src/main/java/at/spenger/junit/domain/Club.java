package at.spenger.junit.domain;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import at.spenger.junit.domain.Person.Sex;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	//Durchschnittsalter berechnen
	public double averageAge() {
		
		if(numberOf() != 0) {
			double d = 0;
			for(Person p : l) {
				d += Period.between(p.getBirthday(), LocalDate.now()).getYears();
			}
			
			return d / numberOf();
		}
		return 0;
	
	}
	//Sortieren der Firstnames
	public List<String> sortieren()
	{
		List<String> namen = new ArrayList<String>();
	
		for (int i = 0; i < l.size(); i++) {
			namen.set(i,l.get(i).getFirstName());
		
	    Collections.sort(namen); 
	   
		}
		return namen;
	}
	//Austritt einer Person
	public boolean austreten(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.remove(p);
	}
	
	//Austritt aller Personen
	public boolean alleAustreten()
	
	{
		return l.removeAll(l);
	}
	
	
	public Person aeltester() {
		
		if(numberOf() != 0) {
			Collections.sort(l, new AgeComparator());
			return l.get(0);
		}
		return null;
	}
	
	
	//Stream Sort Name
	public void sortNameStream()
	{
		l=l.stream()
			.sorted((x,y) -> x.getLastName().compareTo(y.getLastName()))
			.collect(Collectors.toList());
	}
	
	//Stream Sort Alter
	public void sortAlterStream()
	{
		l=l.stream()
				.sorted((x,y) -> x.getBirthdayString().compareTo(y.getBirthdayString()))
				.collect(Collectors.toList());
		

	}
	
	//Stream Sort Alter
		public void sortAlterDateStream()
		{
			l=l.stream()
					.sorted((x,y) -> x.getBirthday().compareTo(y.getBirthday()))
					.collect(Collectors.toList());
			

		}
		
	//Stream Gruppieren mit gleicher Methode. Leider wei� ich nicht wie das funktionieren k�nnte
	public void groupGebrurtsjahrStream()
	{
		
		l=l.stream()
				.sorted((x,y) -> x.getBirthdayString().compareTo(y.getBirthdayString()))
				.collect(Collectors.toList());
		
	}
	
	public float averageStream()
	{
		int count = (int) l.stream().count();
		int sumAlter = l.stream().mapToInt(Person::getAge).sum();
		
		return sumAlter / count;
	
	}
	public int countAnzFrauen()
	{
		return (int)l.stream().filter(p -> p.getSex().equals(Sex.FEMALE)).count();
	}
	
	
	

}

